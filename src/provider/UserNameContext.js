import { createContext } from 'react';
import { getUserName } from '../utils/store';

const UserNameContext = createContext(getUserName)

export default UserNameContext;
