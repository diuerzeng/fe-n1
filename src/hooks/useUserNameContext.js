import { useContext } from 'react';
import UserNameContext from '../provider/UserNameContext';

const useUserNameContext = () => {
  return useContext(UserNameContext);
}

export default useUserNameContext
