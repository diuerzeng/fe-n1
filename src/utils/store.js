

export const getUserName = () => {
  return window.localStorage.getItem('user-name')
}

export const setUserName = userName => {
  window.localStorage.setItem('user-name', userName)
}
