import React from 'react';
import { marked } from "marked";
import { useMount, useSetState } from 'react-use';
import spec from './spec.md'
import imgSrc from '../../assets/layout.png'
import demoSrc from '../../assets/search-form.mov'
import 'github-markdown-css'

const About = () => {
  const userName = window.localStorage.getItem('user-name');
  const [state, setState] = useSetState({ spec: '' })

  useMount(() => {
    fetch(spec)
      .then(response => response.text())
      .then(text => {
        text = text.replace("<layout />", `<img src="${imgSrc}" />`)
        text = text.replace("<demo />", `<video controls="controls" width="1000" height="300" name="Video Name"><source src="${demoSrc}"></video>`)
        setState({
          spec: marked.parse(text)
        })
      })
  });

  return (
    <div className="about">
      <h1>{`歡迎！ 偉大的 乂煞氣a ${userName} 乂`}</h1>
      <div className="markdown-body" dangerouslySetInnerHTML={{ __html: state.spec }} />
    </div>
  )
}

export default About;
