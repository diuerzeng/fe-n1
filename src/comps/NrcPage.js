import React, { Component } from 'react';
import { createForm, NrcForm, NrcFormItem, NrcSelect, NrcInputCalendar, Button } from 'nrc-components';

class NrcPage extends Component {

  state = {
    period: '1'
  }

  render() {
    const { decorateInput } = this.props.form;
    return (
      <NrcForm>
        <NrcFormItem
          labelName={`options`}
        >
          {
            decorateInput('period', {})(
              <NrcSelect
                options={[
                  { label: 'option 1', value: '1' },
                  { label: 'option 2', value: '2' },
                ]}
                onChange={() => { }}
              />
            )
          }
        </NrcFormItem>
        <NrcFormItem
          labelName={`date`}
        >
          {
            decorateInput('date', {
              rule: {
                validator: [
                  (value) => value.startValue && value.endValue,
                ],
                tip: [
                  'not empty!!',
                ],
              }
            })(
              <NrcInputCalendar
                defaultValue={{
                  startValue: 949334400000,
                  endValue: 949334400000,
                }}
                onChange={value => console.log('NrcInputCalendar : ', value)}
              />
            )
          }
        </NrcFormItem>
        <Button onClick={() => { }}>Reset</Button>
        <Button onClick={() => { }}>Search</Button>
      </NrcForm>
    );
  }
}

export default createForm()(NrcPage);
