import React from 'react';
import { useHistory } from 'react-router-dom'
import useUserNameContext from '../hooks/useUserNameContext';
import {
  Formik,
  InputField,
} from 'nrc-formik';
import * as Yup from 'yup';
import logo from '../logo.svg';


const inputSchema = Yup.object({
  userName: Yup.string().nullable().required('沒有姓名無法繼續唷！')
})

const Home = () => {
  const history = useHistory();
  const { name, setName } = useUserNameContext();

  const onSubmit = values => {
    if (values.userName) {
      window.localStorage.setItem('user-name', values.userName);
      setName(values.userName);
      history.push('/about');
    }
  }

  const nextPage = () => {
    history.push('/about');
  }

  return (
    <>
      <img src={logo} className="App-logo" alt="logo" />
      {
        name ? (
          <div className='entry'>
            <button type='button' onClick={nextPage}>就只是個按鈕</button>
          </div>
        ) : (
          <Formik
            initialValues={{ userName: name }}
            validationSchema={inputSchema}
            onSubmit={onSubmit}
          >
            {({ values, setValues, handleSubmit, handleReset }) => {
              return (
                <div className='entry'>
                  <div className='input-use-name'>
                    我是：<InputField name='userName' />
                  </div>
                  <button type='button' onClick={handleSubmit}>送出</button>
                </div>
              )
            }}
          </Formik>
        )
      }
    </>
  )
}

export default Home;
